goog.provide('ota.modulePages.hotel.ComparisonSolutionsAds');

goog.require('ota.modulePages.serp.ComparisonSolutionsAds');
goog.require('ota.MediaAds');


/**
 * @constructor
 * @struct
 * @extends {ota.modulePages.serp.ComparisonSolutionsAds}
 */
ota.modulePages.hotel.ComparisonSolutionsAds = function() {
  goog.base(this);

  /** @protected {string} */
  this.hotelId = '';
};
goog.inherits(ota.modulePages.hotel.ComparisonSolutionsAds, ota.modulePages.serp.ComparisonSolutionsAds);

/** @const {string} */
ota.modulePages.hotel.ComparisonSolutionsAds.EXIT_UNIT_NAME = 'exithotel';

/** @const {string} */
ota.modulePages.hotel.ComparisonSolutionsAds.INLINE_BANNER_NAME = 'hp_inline_banner';

/** @override */
ota.modulePages.hotel.ComparisonSolutionsAds.prototype.setParentComponent = function(hotel) {
  this.parentComponent = hotel;
  this.urlState = hotel.getUrlState();
  this.serpHint = hotel.getSession();

  var request = hotel.getRequest();
  var route = /** @type {ota.pages.hotel.Route} */ (request.route);
  this.hotelId = route.getId(request.uri);

  this.init();
};

/** @override */
ota.modulePages.hotel.ComparisonSolutionsAds.prototype.getExitUnitName = function() {
  return ota.modulePages.hotel.ComparisonSolutionsAds.EXIT_UNIT_NAME;
};

/** @override */
ota.modulePages.hotel.ComparisonSolutionsAds.prototype.getInlineBannersName = function() {
  return ota.modulePages.hotel.ComparisonSolutionsAds.INLINE_BANNER_NAME;
};

/**
 * @private 
 * @param {ota.clicktripz.Compareto} unit
 */
ota.modulePages.hotel.ComparisonSolutionsAds.prototype._setClicktripzHotelId = function(unit) {
  unit.setHotel(this.hotelId, function(hotelId, compareto) {
    compareto.draw();
  });
};

/** @inheritDoc */
ota.modulePages.hotel.ComparisonSolutionsAds.prototype.initClicktripzExitUnit = function() {
  goog.base(this, 'initClicktripzExitUnit');

  this._setClicktripzHotelId(this.getExitUnitCompareto());
};

/** @inheritDoc */
ota.modulePages.hotel.ComparisonSolutionsAds.prototype.initClicktripzInlineBanners = function() {
  goog.base(this, 'initClicktripzInlineBanners');

  this._setClicktripzHotelId(this.getInlineBannersCompareto());
};

/** @override */
ota.modulePages.hotel.ComparisonSolutionsAds.prototype.createInlineBanners = function(ads) {
  ota.MediaAds.decorateWithLogger(function(ads) {
    if (!goog.array.isEmpty(ads)) {
      app.instance.mediaAds.log('inline banner advertises: ' +
          JSON.stringify(ads));
      this.inlineBannersData = ads;
      if (this.parentComponent.getAdsContainer()) {
        this.parentComponent.getAdsContainer().createClicktripzBanner(ads);
      }
    } else {
      app.instance.mediaAds.log('Empty data');
      ota.analytics.funnel.event(
          app.instance.mediaAds.getPartnerFunnelCategory(), 
          'serp_inline_banners_empty');
    }
  }, this, 'Inline banners data')(ads);
};

/** @override */
ota.modulePages.hotel.ComparisonSolutionsAds.prototype.onPopunderComplete = function() {
  ota.analytics.funnel.event(app.instance.mediaAds.getPartnerFunnelCategory(),
      'hotelpage_kubunder_fired');
};

/** @inheritDoc */
ota.modulePages.hotel.ComparisonSolutionsAds.prototype.initClicktripzPopunder = function() {
  goog.base(this, 'initClicktripzPopunder');

  this._setClicktripzHotelId(this.getPopunderCompareto());
};

/** @inheritDoc */
ota.modulePages.hotel.ComparisonSolutionsAds.prototype.getIntentMediaPopunderApiConfig =
    function() {
  return ota.intentmedia.getApiConfig(this.urlState, this.serpHint, this.hotelId);
};

/** @inheritDoc */
ota.modulePages.hotel.ComparisonSolutionsAds.prototype.getImpressionsConfig =
    function(placementCode) {
  var guests = ota.data.queryParser.utils.getGuestsFromRooms(
      this.urlState.getGuests());
  return {
    placementCode: placementCode,
    adults: guests.adults,
    children: guests.ages.length,
    checkin: this.urlState.getFrom(),
    checkout: this.urlState.getTo(),
    rooms: this.urlState.getGuests().length,
    countryCode: this.serpHint.meta.region.countryCode,
    city: this.serpHint.meta.region.nameEn,
    hotelId: this.hotelId
  }
};
