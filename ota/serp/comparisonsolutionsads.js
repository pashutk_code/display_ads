goog.provide('ota.modulePages.serp.ComparisonSolutionsAds');

goog.require('goog.array');
goog.require('goog.events.BrowserEvent.MouseButton');
goog.require('goog.events.EventTarget');
goog.require('ota.analytics.funnel');
goog.require('ota.Clicktripz');
goog.require('ota.clicktripz.Compareto');
goog.require('ota.clicktripz.config');
goog.require('ota.data.mediaAds.BannerAdvertiser');
goog.require('ota.intentmedia');
goog.require('ota.MediaAds');
goog.require('ota.net.cookies');
goog.require('ota.widgets.clicktripz.ExitUnit');


/**
 * @constructor
 * @struct
 * @extends {goog.events.EventTarget}
 */
ota.modulePages.serp.ComparisonSolutionsAds = function() {
  goog.base(this);

  /** @protected {*} */
  this.parentComponent = null;

  /** @protected {ota.modulePages.serp.State} */
  this.urlState = null;

  /** @protected {ota.data.SerpHint?} */
  this.serpHint = null;

  /** @private {ota.MediaAds.Advertiser} */
  this._advertiser = app.instance.mediaAds.getCurrentAdvertiser();

  /** @private {ota.clicktripz.Compareto} */ 
  this._ctComparetoExitUnit = null;

  /** @private {ota.widgets.clicktripz.ExitUnit} */
  this._exitUnit = null;

  /** @protected {Array.<ota.data.mediaAds.BannerAdvertiser>} */
  this.inlineBannersData = [];

  /** @private {ota.clicktripz.Compareto} */
  this._ctComparetoInline = null;

  /** @private {ota.clicktripz.Compareto} */
  this._ctComparetoPopunder = null;

  /** @private {goog.events.EventHandler} */ 
  this._handler = new goog.events.EventHandler(this);
  this.registerDisposable(this._handler);
};
goog.inherits(ota.modulePages.serp.ComparisonSolutionsAds, goog.events.EventTarget);

/**
 * @typedef {{
 *   unitName: string,
 *   onDraw: Function,
 *   unitType: ota.clicktripz.Compareto.UnitType,
 *   opt_maxAdvertisers: (number|undefined)
 * }}
 */
ota.modulePages.serp.ComparisonSolutionsAds.UserIntentUnitConfig;

/**
 * @const
 * @type {number}
 */ 
ota.modulePages.serp.ComparisonSolutionsAds.INLINE_BANNER_MAX_ADVERTISERS = 4;

/** @const {string} */
ota.modulePages.serp.ComparisonSolutionsAds.EXIT_UNIT_NAME = 'exitcity';

/** @const {string} */
ota.modulePages.serp.ComparisonSolutionsAds.INLINE_BANNER_NAME = 'inlinebannerscity';

/** @const {string} */
ota.modulePages.serp.ComparisonSolutionsAds.POPUNDER_NAME = 'city';

/** @param {*} component */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.setParentComponent = function(component) {
  this.parentComponent = component;

  this.urlState = component.getUrlState();
  this.serpHint = component.getSerpHint();

  this.init();
};

ota.modulePages.serp.ComparisonSolutionsAds.prototype.init = function() {
  ota.MediaAds.decorateWithLogger(this.initExitUnit, this,
      this.getExitUnitName(), 'init')();
  ota.MediaAds.decorateWithLogger(this.initInlineBanners, this,
      this.getInlineBannersName(), 'init')();
  ota.MediaAds.decorateWithLogger(this.initPopunder, this,
      this.getPopunderName(), 'init')();
};

ota.modulePages.serp.ComparisonSolutionsAds.prototype.initExitUnit = function() {
  var banned = !app.instance.mediaAds.isUnitAllowedForCurrentPartner(
      ota.MediaAds.UnitSubtype.OVERLAY);
  if (ota.net.cookies.get(ota.widgets.clicktripz.ExitUnit.SHOWED_COOKIE) ||
      banned) {
    app.instance.mediaAds.log('Exit unit was showed or utm ' +
        'source is banned');
    return;
  }

  if (this._advertiser === ota.MediaAds.Advertiser.INTENT_MEDIA) {
    this.initIntentMediaExitUnit();
  } else {
    this.initClicktripzExitUnit();
  }
};

/**
 * @param {ota.intentmedia.PlacementCode} placementCode
 * @returns {ota.intentmedia.ImpressionsConfig}
 */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.getImpressionsConfig =
    function(placementCode) {
  var guests = ota.data.queryParser.utils.getGuestsFromRooms(
      this.urlState.getGuests());
  return {
    placementCode: placementCode,
    adults: guests.adults,
    children: guests.ages.length,
    checkin: this.urlState.getFrom(),
    checkout: this.urlState.getTo(),
    rooms: this.urlState.getGuests().length,
    countryCode: this.serpHint.meta.region.countryCode,
    city: this.serpHint.meta.region.nameEn
  }
};

/** @protected */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.initIntentMediaExitUnit = function() {
  var placementCode = ota.intentmedia.PlacementCode.OVERLAY;
  var impressionsConfig = this.getImpressionsConfig(placementCode);
  ota.intentmedia.getImpressionsAds(impressionsConfig).
      then(this.createExitUnit, this.onIntentmediaExitUnitFailed, this);
};

ota.modulePages.serp.ComparisonSolutionsAds.prototype.onIntentmediaExitUnitFailed =
    function(err) {
  app.instance.mediaAds.log('Intent media exit unit fail', err);
};

/** @returns {ota.clicktripz.Compareto} */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.getExitUnitCompareto = function() {
  return this._ctComparetoExitUnit;
};

ota.modulePages.serp.ComparisonSolutionsAds.prototype.initClicktripzExitUnit = function() {
  var config = {
    unitName: this.getExitUnitName(),
    onDraw: this._createClilcktripzExitUnit,
    unitType: ota.clicktripz.Compareto.UnitType.OVERLAY
  };
  this._ctComparetoExitUnit = this.initClicktripzUserIntentUnit(config);
};

/**
 * @param {ota.clicktripz.Compareto.Event} evt
 * @private
 */
ota.modulePages.serp.ComparisonSolutionsAds.prototype._createClilcktripzExitUnit = 
    function(evt) {
  this.createExitUnit(evt.data);
};

/** 
 * @protected
 * @param {ota.widgets.clicktripz.ExitUnit} exitUnit
 */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.setExitUnit = function(exitUnit) {
  this._exitUnit = exitUnit;
};

/**
 * @param {Array.<ota.data.mediaAds.BannerAdvertiser>} ads
 * @protected
 */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.createExitUnit = function(ads) {
  ota.MediaAds.decorateWithLogger(function(ads) {
    var exitUnit;
    if (!goog.array.isEmpty(ads)) {
      app.instance.mediaAds.log('data: ' + JSON.stringify(ads));
      exitUnit = new ota.widgets.clicktripz.ExitUnit(ads);
      this.setExitUnit(exitUnit);
      this.registerDisposable(exitUnit);
      exitUnit.render(this.parentComponent.getContainer().getElement());
    } else {
      app.instance.mediaAds.log('Empty data');
      ota.analytics.funnel.event(
          app.instance.mediaAds.getPartnerFunnelCategory(), 'overlay_empty');
    }
  }, this, 'Exit unit data')(ads);
};

/**
 * @param {ota.modulePages.serp.ComparisonSolutionsAds.UserIntentUnitConfig} config
 * @return {ota.clicktripz.Compareto}
 */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.initClicktripzUserIntentUnit =
    function(config) {
  ota.Clicktripz.init();
  var unit = new ota.clicktripz.Compareto(config.unitName);
  this.registerDisposable(unit);
  this._handler.listenOnce(unit, ota.clicktripz.Compareto.EventType.DRAW,
      config.onDraw);
  if (goog.isDefAndNotNull(config.opt_maxAdvertisers)) {
    unit.setMaxAdvertisers(config.opt_maxAdvertisers);
  }
  unit.
      setRedirectUrlByType(config.unitType).
      setForcePopup().
      setUserIntentMode().
      setUrlState(this.urlState).
      setSerpHint(this.serpHint).
      draw();
  return unit;
};

ota.modulePages.serp.ComparisonSolutionsAds.prototype.initInlineBanners = function() {
  var banned = !app.instance.mediaAds.isUnitAllowedForCurrentPartner(
      ota.MediaAds.UnitSubtype.INLINE_BANNER);
  if (banned) {
    app.instance.mediaAds.log('Inline banners are banned');
    return;
  }
  
  switch (this._advertiser) {
    case ota.MediaAds.Advertiser.CLICKTRIPZ:
      this.initClicktripzInlineBanners();
      break;
    case ota.MediaAds.Advertiser.INTENT_MEDIA:
      this.initIntentmediaInlineBanners();
  }
};

/** @returns {string} */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.getInlineBannersName = function() {
  return ota.modulePages.serp.ComparisonSolutionsAds.INLINE_BANNER_NAME;
};

/** @returns {ota.clicktripz.Compareto} */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.getInlineBannersCompareto =
    function() {
  return this._ctComparetoInline;
};

ota.modulePages.serp.ComparisonSolutionsAds.prototype.initClicktripzInlineBanners =
    function() {
  var config = {
    unit: this.getInlineBannersCompareto(),
    unitName: this.getInlineBannersName(),
    onDraw: this.createClicktripzInlineBanners,
    unitType: ota.clicktripz.Compareto.UnitType.INLINE_BANNER,
    opt_maxAdvertisers:
        ota.modulePages.serp.ComparisonSolutionsAds.INLINE_BANNER_MAX_ADVERTISERS
  };
  this._ctComparetoInline = this.initClicktripzUserIntentUnit(config);
};

ota.modulePages.serp.ComparisonSolutionsAds.prototype.initIntentmediaInlineBanners =
    function() {
  var placementCode = ota.intentmedia.PlacementCode.INTERCARD;
  var impressionsConfig = this.getImpressionsConfig(placementCode);
  ota.intentmedia.getImpressionsAds(impressionsConfig).
      then(this.createInlineBanners,this.onIntentmediaInlineBannersFailed, this);
};

ota.modulePages.serp.ComparisonSolutionsAds.prototype.onIntentmediaInlineBannersFailed =
    function(err) {
  app.instance.mediaAds.log('Intent media inline banners fail', err);
};

ota.modulePages.serp.ComparisonSolutionsAds.prototype.createClicktripzInlineBanners
    = function(evt) {
  this.createInlineBanners(evt.data);
};

/**
 * Creating inline banners
 * @param {ota.clicktripz.Compareto.Event} ads
 * @private
 */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.createInlineBanners = function(ads) {
  ota.MediaAds.decorateWithLogger(function(ads) {
    if (!goog.array.isEmpty(ads)) {
      app.instance.mediaAds.log('data: ' + JSON.stringify(ads));
      this.inlineBannersData = ads;
      this.parentComponent.rerenderHotels();
    } else {
      app.instance.mediaAds.log('Empty data');
      ota.analytics.funnel.event(
          app.instance.mediaAds.getPartnerFunnelCategory(),
          'serp_inline_banners_empty');
    }
  }, this, 'Inline banners data')(ads);
};

ota.modulePages.serp.ComparisonSolutionsAds.prototype.initPopunder = function() {
  var banned = !app.instance.mediaAds.isUnitAllowedForCurrentPartner(
      ota.MediaAds.UnitSubtype.POPUNDER);
  if (ota.net.cookies.get(ota.clicktripz.config.WAS_FORCE_SHOW_COOKIE) ||
      banned) {
    app.instance.mediaAds.log('Popunder WAS_FORCE_SHOW_COOKIE or banned');
    return;
  }

  switch (this._advertiser) {
    case ota.MediaAds.Advertiser.CLICKTRIPZ:
      this.initClicktripzPopunder();
      break;
    case ota.MediaAds.Advertiser.INTENT_MEDIA:
      this._initIntentMediaPopunder();
      break;
  }
};

ota.modulePages.serp.ComparisonSolutionsAds.prototype.initClicktripzPopunder =
    function() {
  ota.Clicktripz.init();
  this._ctComparetoPopunder = new ota.clicktripz.Compareto(
      this.getPopunderName());
  this.registerDisposable(this._ctComparetoPopunder);
  this._handler.
      listen(goog.global.document.body, goog.events.EventType.CLICK, 
          this._bodyClickHandler).
      listen(this._ctComparetoPopunder,
          ota.clicktripz.Compareto.EventType.COMPLETE, this.onPopunderComplete);
  this._ctComparetoPopunder.
      setUrlState(this.urlState).
      setSerpHint(this.serpHint).
      setRedirectUrlByType(ota.clicktripz.Compareto.UnitType.POPUNDER).
      draw();
};

ota.modulePages.serp.ComparisonSolutionsAds.prototype.onPopunderComplete = function() {
  ota.analytics.funnel.event(app.instance.mediaAds.getPartnerFunnelCategory(),
      'serp_kubunder_fired');
};

/**
 * @private
 * @param {goog.events.BrowserEvent} evt
 */
ota.modulePages.serp.ComparisonSolutionsAds.prototype._bodyClickHandler = function(evt) {
  if (evt.isButton(goog.events.BrowserEvent.MouseButton.LEFT) &&
      !(evt.metaKey || evt.ctrlKey)) {
    this._ctComparetoPopunder.submit();
  }
};

/** 
 * @protected
 * @return {ota.intentmedia.ApiConfig}
 */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.getIntentMediaPopunderApiConfig =
    function() {
  return ota.intentmedia.getApiConfig(this.urlState, this.serpHint);
};

/** @private */
ota.modulePages.serp.ComparisonSolutionsAds.prototype._initIntentMediaPopunder =
    function() {
  var apiConfig = this.getIntentMediaPopunderApiConfig();
  var properties = ota.intentmedia.getCoreProperties(apiConfig);
  ota.intentmedia.setIntentMediaPropertiesGlobally(properties);
  ota.intentmedia.loadCore(
      this.intentCoreResolveHandler, this.intentCoreRejectHandler, this);
};

/** @protected */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.intentCoreResolveHandler =
    function() {
  app.instance.mediaAds.log('Intent media Core script loaded');
};

/** @protected */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.intentCoreRejectHandler =
    function(error) {
  app.instance.mediaAds.log('Intent media Core script error', error);
};

/**  @return {Array.<ota.data.mediaAds.BannerAdvertiser>} data */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.getInlineBannersData = function() {
  return this.inlineBannersData;
};

/** @return {string} */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.getExitUnitName = function() {
  return ota.modulePages.serp.ComparisonSolutionsAds.EXIT_UNIT_NAME;
};

/** @return {string} */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.getPopunderName = function() {
  return ota.modulePages.serp.ComparisonSolutionsAds.POPUNDER_NAME
};

/** @returns {ota.clicktripz.Compareto} */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.getPopunderCompareto = function() {
  return this._ctComparetoPopunder;
};

/** @inheritDoc */
ota.modulePages.serp.ComparisonSolutionsAds.prototype.disposeInternal = function() {
  goog.base(this, 'disposeInternal');

  this.parentComponent = null;
  this.urlState = null;
  this.serpHint = null;
  this._ctComparetoExitUnit = null;
  this._exitUnit = null;
  this._ctComparetoInline = null;
  this._handler = null;
  this.inlineBannersData = null;
  this._ctComparetoPopunder = null;
};
