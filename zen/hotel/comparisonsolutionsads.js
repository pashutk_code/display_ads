goog.provide('zen.ui.roomsPage.ComparisonSolutionsAds');

goog.require('ota.modulePages.hotel.ComparisonSolutionsAds');


/**
 * @constructor
 * @struct
 * @extends {ota.modulePages.hotel.ComparisonSolutionsAds}
 */
zen.ui.roomsPage.ComparisonSolutionsAds = function() {
  goog.base(this);
};
goog.inherits(zen.ui.roomsPage.ComparisonSolutionsAds, ota.modulePages.hotel.ComparisonSolutionsAds);

/** @override */
zen.ui.roomsPage.ComparisonSolutionsAds.prototype.setParentComponent = function(roomspage) {
  this.parentComponent = roomspage;
  this.urlState = roomspage.data.urlState;
  this.serpHint = roomspage.manager.serpHint;
  this.hotelId = roomspage.data.hotel.id;

  this.init();
};

/** @override */
zen.ui.roomsPage.ComparisonSolutionsAds.prototype.init = function() {
  ota.MediaAds.decorateWithLogger(this.initExitUnit, this,
      this.getExitUnitName(), 'init')();
  ota.MediaAds.decorateWithLogger(this.initPopunder, this,
      this.getPopunderName(), 'init')();
};

/** @override */
zen.ui.roomsPage.ComparisonSolutionsAds.prototype.createExitUnit = function(ads) {
  ota.MediaAds.decorateWithLogger(function(ads) {
    var exitUnit;
    if (!goog.array.isEmpty(ads)) {
      app.instance.mediaAds.log('data: ' + JSON.stringify(ads));
      exitUnit = new ota.widgets.clicktripz.ExitUnit(ads);
      this.setExitUnit(exitUnit);
      this.registerDisposable(exitUnit);
      exitUnit.render(this.parentComponent.getElement());
    } else {
      app.instance.mediaAds.log('Empty data');
      ota.analytics.funnel.event(
          app.instance.mediaAds.getPartnerFunnelCategory(), 'overlay_empty');
    }
  }, this, 'Exit unit data')(ads);
};
