goog.provide('zen.ui.hotels.ComparisonSolutionsAds');

goog.require('ota.modulePages.serp.ComparisonSolutionsAds');


/**
 * @constructor
 * @struct
 * @extends {ota.modulePages.serp.ComparisonSolutionsAds}
 */
zen.ui.hotels.ComparisonSolutionsAds = function() {
  goog.base(this);
};
goog.inherits(zen.ui.hotels.ComparisonSolutionsAds, ota.modulePages.serp.ComparisonSolutionsAds);

/** @override */
zen.ui.hotels.ComparisonSolutionsAds.prototype.setParentComponent = function(hotelspage) {
  this.parentComponent = hotelspage;
  this.urlState = hotelspage.getUrlState();
  this.serpHint = hotelspage.getSerpHint();
  
  this.init();
};

/** @override */
zen.ui.hotels.ComparisonSolutionsAds.prototype.init = function() {
  ota.MediaAds.decorateWithLogger(this.initExitUnit, this,
    this.getExitUnitName(), 'init')();
  ota.MediaAds.decorateWithLogger(this.initPopunder, this,
    this.getPopunderName(), 'init')();
};

/** @override */
zen.ui.hotels.ComparisonSolutionsAds.prototype.createExitUnit = function(ads) {
  ota.MediaAds.decorateWithLogger(function(ads) {
    if (!goog.array.isEmpty(ads)) {
      app.instance.mediaAds.log('data: ' + JSON.stringify(ads));
      var exitUnit = new ota.widgets.clicktripz.ExitUnit(ads);
      this.setExitUnit(exitUnit);
      this.registerDisposable(exitUnit);

      var page = /** @type {zen.pages.Hotels} */ (
          app.instance.pageManager.getCurrentPage());
      var element = page.getContainer().getElement();
      exitUnit.render(element);
    } else {
      app.instance.mediaAds.log('Empty data');
      ota.analytics.funnel.event(
          app.instance.mediaAds.getPartnerFunnelCategory(), 'overlay_empty');
    }
  }, this, 'Exit unit data')(ads);
};
